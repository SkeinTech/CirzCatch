import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { CommondialoguesProvider } from '../../providers/commondialogues/commondialogues';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public dialog : CommondialoguesProvider,
    public events: Events) {
  }
  ionViewCanEnter() {
    console.log('ionViewDidLoad HomePage');
    this.events.publish('user:updated');
  }
  /**
   * Method for navigating pages
   * based on root settings, pages will be setroot  or push into navigation stack
   * @param data
   * @param root
   */
  navigatePages(data,root){
    console.log("Page data is",data);
    // (data == 'Settings' || data == 'Profile') ? 
    // this.dialog.showAlertPopup("Information","Currently We are working on this feature, it will be comming soon!!!"):
    (root == "yes")? this.navCtrl.setRoot(data) : this.navCtrl.push(data);
  }
}
