import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, RadioButton, Checkbox } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AlertController } from 'ionic-angular';
import { CommonstorageProvider } from '../../providers/commonstorage/commonstorage';
import { CommondialoguesProvider } from '../../providers/commondialogues/commondialogues';
import { CommonutilsProvider } from './../../providers/commonutils/commonutils';
import { CommonservicesProvider } from '../../providers/commonservices/commonservices';

/**
 * @author Karthikeyan.K
 * Date : 17.02.2018
 * 
 */
@IonicPage()
@Component({
  selector: 'page-scoreboard',
  templateUrl: 'scoreboard.html',
})
export class ScoreboardPage {
  loginData:any;
  login:any;
  score: number;
  temp_batsman: any[] = [];
  temp_bowler: any[] = [];
  team_A_players: any;
  team_B_players: any;
  batsman_1_id: number;
  batsman_1_name: string = "";
  batsman_1_runs: number;
  batsman_1_balls: number;
  batsman_2: any;
  batsman_2_id: number;
  batsman_2_name: string = "";
  batsman_2_runs: number;
  batsman_2_balls: number;
  current_batsman_id: number;
  current_batsman_name: string;
  current_batsman_runs: number;
  current_batsman_balls: number;
  bowler_id: number;
  bowler_name: string = "";
  prev_bowler_id: number;
  current_bowler_runs: number;
  current_bowler_over: number;
  current_bowler_wicket: number;
  wickets: number;
  overs: number;
  batting_team: string;
  batting_team_id: string;
  bowling_team: string;
  bowling_team_id: string;
  current_ball_score: string = "";
  extra_run: boolean;
  next_ball: boolean;
  on_strike: boolean;
  over: string[] = [];
  run_entered: number = 0;
  extra_entered: number = 0;
  retired_batsman: string = "";
  match_details: any=[];
  temp_match_details: any;
  toss_detail: any = "";
  temp_1: number;
  temp_2: number;
  temp_3: number;
  match_status: string = "";
  innings = "first";
  result: string;
  final_result: string;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public alertCtrl: AlertController,
    public dialogues: CommondialoguesProvider,
    public storage: CommonstorageProvider,
    public utils: CommonutilsProvider,
    public service: CommonservicesProvider) {
    this.temp_match_details = this.navParams.get("matchdet");
    console.log(this.temp_match_details);
  }
  ionViewCanEnter() {
    /**
     * Checks the User login Credentials
     */
    this.dialogues.showLoading("Checking your Match Details to proceed");
    this.storage.decryptandRead("loginData").then((respData:any)=>{
      this.login = JSON.parse(respData);
      this.loginData = (this.login) ? this.login[0] : "";
      (this.loginData == "")?this.navCtrl.setRoot("RegistrationPage"):"";
      this.check_matchdetails();
    });
  }

  /**
   * Checks the Match Details of the User
   */
  check_matchdetails() {
    this.storage.decryptandRead("match_details").then((data: any) => {
      if (data != null) {
        this.match_details = JSON.parse(data) || [];
        console.log(this.match_details);
        if (this.temp_match_details != undefined) {
          if (this.temp_match_details.match_id != this.match_details.match_id) {
            this.dialogues.dismissLoader();
            this.dialogues.showAlertPopup("Match Info", "Please Wait Until the Running match to finish");
            this.navCtrl.setRoot("HomePage");
          } else {
            this.dialogues.dismissLoader();
            this.match_details = JSON.parse(data);
            this.storage.encryptandStore("match_details", this.match_details).then(() => {
              this.check_toss();
            });
          }
        }
        else {
          this.check_toss();
        }
      }
      else {
        if (this.temp_match_details == undefined) {
          this.dialogues.dismissLoader();
          this.dialogues.showAlertPopup("Match Info", "Please Proceed the Match in Match Details");
          this.navCtrl.setRoot("MatchDetailsPage");
        }
        else {
          this.dialogues.dismissLoader();
          this.match_details = this.temp_match_details;
          this.storage.encryptandStore("match_details", this.match_details).then(() => {
            console.log(this.match_details);
            this.check_toss();
          });
        }
      }
    });
  }

  /**
   * Checks the Toss Details of the Match
   */
  check_toss() {
    let serve = this.utils.getApiConfigs("toss_get");
    this.service.invokeService(serve[0].method, serve[0].path + this.match_details.match_id, '').then((respData: any) => {
      if (respData.code == 200) {
        if (respData.payload == null) {
          this.dialogues.dismissLoader();
          this.dialogues.showAlertPopup("Toss Info", "Please Begin Toss and Proceed back again");
          this.navCtrl.setRoot("HomePage");
        } else {
          this.toss_detail = respData.payload[0];
          this.checks_backup();
        }
      }
    });
  }

  /**
   * Check if any previous backup is available for the running match
   */
  checks_backup() {
    this.storage.decryptandRead("complete_score").then((data: any) => {
      if (data != null) {
        console.log("Getting into complete_score backup code");
        this.restore_values();
      }
      else {
        this.player_values(this.match_details.team_A_id, this.match_details.team_A_name).then((Data: any) => {
          if (Data) {
            this.team_A_players = Data;
            (this.team_A_players != undefined && this.team_B_players != undefined) ? this.innings_check() : "";
          }
        });
        this.player_values(this.match_details.team_B_id, this.match_details.team_B_name).then((BData: any) => {
          if (BData) {
            this.team_B_players = BData;
            (this.team_A_players != undefined && this.team_B_players != undefined) ? this.innings_check() : "";
          }
        });
      }
    });
  }

  /**
   * Function manages the innings of the match
   */
  innings_check() {
    let serve = this.utils.getApiConfigs("scoreboard_get");
    let pay: any = {
      "match_id": this.match_details.match_id
    };
    return new Promise((resolve) => {
      this.service.invokeService(serve[0].method, serve[0].path, pay).then((respData: any) => {
        if (respData.code == 200) {
          let data_check = respData.payload[0];
          if (respData.payload[0].status == "finished") {
            this.innings = "second";
          }
        }
        this.initiate();
      });
    });
  }

  /**
   * 
   * @param id_value Team_id 
   * @param name_value Team_name
   * @description This Function manages the api call to get the respected player values
   */
  player_values(id_value, name_value) {
    let playersList: any[];
    let servicePath = this.utils.getApiConfigs("teamGet");
    let payload: any = { "team_id": id_value, "team_name": name_value };
    return new Promise((resolve) => {
      this.service.invokeService(servicePath[0].method, servicePath[0].path, payload).then((respVal: any) => {
        playersList = respVal.payload;
        resolve(playersList);
      });
    });
  }

  /**
   * 
   * @param matchID  Current Match ID
   * @param teamId Batting Team ID
   * @param teamname Batting Team Name
   * @param call HTTP Method type
   * @param value Innings variate Flag value
   * @description Function used to make Scoreboard_api calls with server
   * 
   */
  scoreboard_api(matchID, teamId, teamname, call, value) {
    let serve = this.utils.getApiConfigs("scoreboard_" + call);
    let pay: any;
    if (value == 0) {
      pay = {
        "match_id": matchID,
        "team_id": teamId,
        "team_name": teamname,
        "score": (call == "put") ? this.score : 0,
        "wicket": (call == "put") ? this.wickets : 0,
        "status": "started",
        "flag": value
      };
    }
    else {
      pay = {
        "match_id": matchID,
        "team_2_id": teamId,
        "team_2_name": teamname,
        "score_two": (call == "put") ? this.score : 0,
        "wicket_two": (call == "put") ? this.wickets : 0,
        "finalstatus": "innings",
        "flag": value
      };
    }
    let payload = (call == "get") ? "" : pay;
    this.service.invokeService(serve[0].method, serve[0].path, payload).then((respData: any) => {
      if (respData.code == 200) {
        console.log("Output response", respData);
        this.dialogues.dismissLoader();
      }
    });
  }

  /** 
   * @description This Function manages the innings and batting team details of the match
  */
  initiate() {
    if (this.toss_detail.team_id == this.match_details.team_A_id) {
      if (this.toss_detail.choice == "Batting") {
        if (this.innings == "first") {
          this.batting_team_id = this.match_details.team_A_id;
          this.bowling_team_id = this.match_details.team_B_id;
          this.batting_team = this.match_details.team_A_name;
          this.bowling_team = this.match_details.team_B_name;
          this.initiating_values(0);
        }
        else {
          if (this.innings == "second") {
            this.batting_team_id = this.match_details.team_B_id;
            this.bowling_team_id = this.match_details.team_A_id;
            this.batting_team = this.match_details.team_B_name;
            this.bowling_team = this.match_details.team_A_name;
            this.initiating_values(1);
          }
        }
      } else {
        if (this.innings == "first") {
          this.batting_team_id = this.match_details.team_B_id;
          this.bowling_team_id = this.match_details.team_A_id;
          this.batting_team = this.match_details.team_B_name;
          this.bowling_team = this.match_details.team_A_name;
          this.initiating_values(1);
        }
        else
          if (this.innings == "second") {
            this.batting_team_id = this.match_details.team_A_id;
            this.bowling_team_id = this.match_details.team_B_id;
            this.batting_team = this.match_details.team_A_name;
            this.bowling_team = this.match_details.team_B_name;
            this.initiating_values(0);
          }
      }
    } else {
      if (this.toss_detail.choice == "Batting") {
        if (this.innings == "first") {
          this.batting_team_id = this.match_details.team_B_id;
          this.bowling_team_id = this.match_details.team_A_id;
          this.batting_team = this.match_details.team_B_name;
          this.bowling_team = this.match_details.team_A_name;
          this.initiating_values(1);
        }
        else {
          if (this.innings == "second") {
            this.batting_team_id = this.match_details.team_A_id;
            this.bowling_team_id = this.match_details.team_B_id;
            this.batting_team = this.match_details.team_A_name;
            this.bowling_team = this.match_details.team_B_name;
            this.initiating_values(0);
          }
        }
      } else {
        if (this.innings == "first") {
          this.batting_team_id = this.match_details.team_A_id;
          this.bowling_team_id = this.match_details.team_B_id;
          this.batting_team = this.match_details.team_A_name;
          this.bowling_team = this.match_details.team_B_name;
          this.initiating_values(0);
        }
        else {
          if (this.innings == "first") {
            this.batting_team_id = this.match_details.team_B_id;
            this.bowling_team_id = this.match_details.team_A_id;
            this.batting_team = this.match_details.team_B_name;
            this.bowling_team = this.match_details.team_A_name;
            this.initiating_values(1);
          }
        }
      }
    }
  }

  /**
   * @function
   * Initialization of api values with the local vaiables
   */
  initiating_values(value) {
    this.score = 0;
    this.batsman_1_id = null;
    this.batsman_2_id = null;
    this.bowler_id = null;
    this.batsman_1_runs = 0;
    this.batsman_2_runs = 0;
    this.batsman_1_balls = 0;
    this.batsman_2_balls = 0;
    this.current_bowler_runs = 0;
    this.current_bowler_over = 0
    this.current_bowler_wicket = 0;
    this.wickets = 0;
    this.overs = 0.0
    this.on_strike = true;
    this.current_batsman_runs = this.batsman_1_runs;
    this.current_batsman_balls = this.batsman_1_balls;
    let current_value = value;
    switch (current_value) {
      case 0:
        {
          let length_1 = this.team_A_players.length;
          let length_2 = this.team_B_players.length;
          if (length_1 > 11) {
            length_1 = 11;
          }
          if (length_2 > 11) {
            length_2 = 11;
          }
          for (let i = 0; i < length_1; i++) {
            let temp: string = '{"id":' + this.team_A_players[i].id + ',"player":"' + this.team_A_players[i].first_name + ' ' + this.team_A_players[i].last_name + '","status": "","run": 0,"balls": 0}';
            let temp_store: any = JSON.parse(temp);
            this.temp_batsman.push(temp_store);
          }
          for (let i = 0; i < length_2; i++) {
            let temp: string = '{"id":' + this.team_B_players[i].id + ',"player":"' + this.team_B_players[i].first_name + ' ' + this.team_B_players[i].last_name + '","over" : 0,"run" : 0,"wicket": 0 }';
            let temp_store: any = JSON.parse(temp);
            this.temp_bowler.push(temp_store);
          }
          let value: any;
          (this.innings == "first") ? value = 0 : value = 1;
          this.scoreboard_api(this.match_details.match_id, this.match_details.team_A_id, this.match_details.team_A_name, "post", value);
          break;
        }
      case 1:
        {
          console.log(this.batting_team);
          let length_1 = this.team_B_players.length;
          let length_2 = this.team_A_players.length;
          if (length_1 > 11) {
            length_1 = 11;
          }
          if (length_2 > 11) {
            length_2 = 11;
          }
          for (let i = 0; i < length_1; i++) {
            let temp: string = '{"id":' + this.team_B_players[i].id + ',"player":"' + this.team_B_players[i].first_name + ' ' + this.team_B_players[i].last_name + '","status": "","run": 0,"balls": 0}';
            let temp_store: any = JSON.parse(temp);
            this.temp_batsman.push(temp_store);
          }
          for (let i = 0; i < length_2; i++) {
            let temp: string = '{"id":' + this.team_A_players[i].id + ',"player":"' + this.team_A_players[i].first_name + ' ' + this.team_A_players[i].last_name + '","over" : 0,"run" : 0,"wicket": 0 }';
            let temp_store: any = JSON.parse(temp);
            this.temp_bowler.push(temp_store);
          }
          let value: any;
          (this.innings == "first") ? value = 0 : value = 1;
          this.scoreboard_api(this.match_details.match_id, this.match_details.team_B_id, this.match_details.team_B_name, "post", value);
          break;
        }
    }
  }

  /**
   * This Function Stores the player Details each over in Local Storage 
   */
  storing_values() {
    let batsmans: string = '{"player1_id":' + this.batsman_1_id + ',"player1_name":"' + this.batsman_1_name + '","player1_runs":' + this.batsman_1_runs + ',"player1_balls":' + this.batsman_1_balls +
      ',"player2_id":' + this.batsman_2_id + ',"player2_name":"' + this.batsman_2_name + '","player2_runs":' + this.batsman_2_runs + ',"player2_balls":' + this.batsman_2_balls + ',"strike":' + this.on_strike + '}';
    let batsmans_update: any = JSON.parse(batsmans);
    this.storage.encryptandStore("batsmans_update", batsmans_update);
    let bowlers: string = '{"player_id":' + this.bowler_id + ',"player_name":"' + this.bowler_name + '","overs":' + this.current_bowler_over + ',"runs":' + this.current_bowler_runs + ',"wickets":' + this.current_bowler_wicket + '}';
    let bowler_updates: any = JSON.parse(bowlers);
    this.storage.encryptandStore("bowler_updates", bowler_updates);
    let scores: string = '{"batting":"' + this.batting_team + '","bowling":"' + this.bowling_team + '","score":' + this.score + ',"wickets":' + this.wickets + ',"overs":' + this.overs + ',"innings":"' + this.innings + '"}';
    let complete_score: any = JSON.parse(scores);
    this.storage.encryptandStore("complete_score", complete_score);
    // let temp_batting:string = '{"store":"'+this.temp_batsman+'"}';
    let complete_batting: any = JSON.stringify(this.temp_batsman);
    this.storage.encryptandStore("batting", complete_batting);
    // let temp_bowling:string = '{"store":"'+this.temp_bowler+'"}';
    let complete_bowling: any = JSON.stringify(this.temp_bowler);
    this.storage.encryptandStore("bowling", complete_bowling);
    let value: any;
    (this.innings == "first") ? value = 0 : value = 1;
    if(this.overs != this.match_details.overs){
    this.scoreboard_api(this.match_details.match_id, this.batting_team_id, this.batting_team, "put", value);
    }

  }

  /**
   * Function Restores the module which has any previous stored values in local 
   */
  restore_values() {
    console.log("Value is been Restored");
    this.storage.decryptandRead("batsmans_update").then((data: any) => {
      var data_value: any = JSON.parse(data);
      this.batsman_1_id = data_value.player1_id;
      this.temp_1 = data_value.player1_id;
      this.batsman_1_name = data_value.player1_name;
      this.batsman_1_runs = data_value.player1_runs;
      this.batsman_1_balls = data_value.player1_balls;
      this.batsman_2_id = data_value.player2_id;
      this.temp_2 = data_value.player2_id;
      this.batsman_2_name = data_value.player2_name;
      this.batsman_2_runs = data_value.player2_runs;
      this.batsman_2_balls = data_value.player2_balls;
      this.on_strike = data_value.strike;
      this.batsman_update();
    });
    this.bowler_id = null;
    this.current_bowler_runs = 0;
    this.current_bowler_over = 0
    this.current_bowler_wicket = 0;
    // this.storage.decryptandRead("bowler_updates").then((data: any) => {
    //   var data_value: any = JSON.parse(data);
    //   console.log(data_value);
    //   this.prev_bowler_id=data_value.prev_bowler_id;
    //   this.bowler_id = data_value.player_id;
    //   this.bowler_name = data_value.player_name;
    //   this.current_bowler_over = data_value.overs;
    //   this.current_bowler_runs = data_value.runs;
    //   this.current_bowler_wicket = data_value.wickets;
    //   console.log(data_value);
    // });
    this.storage.decryptandRead("complete_score").then((data: any) => {
      var data_value: any = JSON.parse(data);
      this.bowling_team = data_value.bowling;
      this.batting_team = data_value.batting;
      this.score = data_value.score;
      this.wickets = data_value.wickets;
      this.overs = data_value.overs;
      this.innings = data_value.innings;
    });
    this.storage.decryptandRead("batting").then((data: any) => {
      if (data != null) {
        this.temp_batsman = JSON.parse(data);
      }
    });
    this.storage.decryptandRead("bowling").then((data: any) => {
      if (data != null) {
        this.temp_bowler = JSON.parse(data);
      }
    });
    let serve = this.utils.getApiConfigs("scoreboard_get");
    let pay: any = {
      "match_id": this.match_details.match_id
    };
    this.service.invokeService(serve[0].method, serve[0].path, pay).then((respData: any) => {
      if (respData.code == 200) {
        console.log("Output response", respData);
      }
    });
    this.dialogues.dismissLoader();
  }

  /**
   * 
   * @param value 
   * Check whether the run is clicked at once
   */
  run_tap(value) {
    if (this.run_entered == 0) {
      this.run_update(value);
      this.run_entered += 1;
    }
  }

  /**
   * @function
   * @param value 
   * Display the value which is been chosen by the user for the current ball.
   */
  run_update(value) {
    switch (value) {
      case 0: {
        this.current_ball_score += '0';
        break;
      }
      case 1: {
        this.current_ball_score += '1';
        break;
      }
      case 2: {
        this.current_ball_score += '2';
        break;
      }
      case 3: {
        this.current_ball_score += '3';
        break;
      }
      case 4: {
        this.current_ball_score += '4';
        break;
      }
      case 5: {
        this.current_ball_score += '5';
        break;
      }
      case 6: {
        this.current_ball_score += '6';
        break;
      }
    }

  }

  /**
   * @function
   * @param value
   * Checks whether multiple clicks has been made in the extras
   */
  extras_tap(value) {
    if (this.extra_entered == 0) {
      this.extras_update(value);
      this.extra_entered += 1;
    }
  }

  /**
   * @function
   * @param value 
   * Displays which extra is been selected
   */
  extras_update(value) {
    switch (value) {
      case 'w': {
        this.current_ball_score += 'w';
        break;
      }
      case 'n': {
        this.current_ball_score += 'n';
        break;
      }
      case 'b': {
        this.current_ball_score += 'b';
        break;
      }
      case 'l': {
        this.current_ball_score += 'l';
        break;
      }
    }
  }

  /**
   * @function
   * @param value
   * Function for the Wickets update
   */
  player_out(value) {
    switch (value) {
      case 'H': {
        this.current_ball_score += 'H';
        break;
      }
      case 'O': {
        this.current_ball_score += 'O';
        break;
      }
      case 'W': {
        // if ((this.temp_batsman.length - 1)<1) {
          this.current_ball_score += 'W';
        //   console.log(this.current_batsman_name);
        // } else {
        //   this.current_ball_score += 'W';
        //   this.result = "completed";
        //   this.match_finish();
        // }
      }
    }
  }
  
  /** 
   * @function
   * Checks whether the display value is empty.
  */
  update() {
    if (this.current_ball_score != "") {
      if (this.batsman_1_id != null && this.batsman_2_id != null) {
        if (this.bowler_id != null) {
          if (this.current_ball_score != "b" && this.current_ball_score != "l") {
            this.score_update();
          }
        }
        else {
          this.dialogues.showAlertPopup("Select Bowler", "Please Select the Bowler");
        }
      }
      else {
        this.dialogues.showAlertPopup("Select Batsman", "Please Select the Batsman");
      }
    }
  }

  /**
   * @function
   * Clears the display value
   */
  clear_value() {
    this.current_ball_score = "";
    this.run_entered = 0;
    this.extra_entered = 0;
  }

  /** 
   * @function
   * Score Board complete Updation function 
  */
  score_update() {
    let index: number = this.current_ball_score.length;

    /**
     * checks Whether it is extra run
     */
    if (this.current_ball_score.indexOf('w') != -1 || this.current_ball_score.indexOf('l') != -1 || this.current_ball_score.indexOf('b') != -1) {
      this.extra_run = true;
    }
    else {
      this.extra_run = false;
    }

    /**
     * Checks whether the ball is correct or need to rebowl it.
     */
    if (this.current_ball_score.indexOf('w') != -1 || this.current_ball_score.indexOf('n') != -1 || this.current_ball_score == "H") {
      this.next_ball = false;
    } else {
      this.next_ball = true;
    }

    for (let i = 0; i < index; i++) {
      console.log(this.current_ball_score.charAt(i));
      switch (this.current_ball_score.charAt(i)) {
        case 'w': {
          this.score += 1;
          this.current_bowler_runs += 1;
          break;
        }
        case 'n': {
          this.score += 1;
          this.current_bowler_runs += 1;
          break;
        }
        case 'H': {
          this.player_wicket('Retired-Hut');
          break;
        }
        case 'W': {
          this.current_bowler_wicket += 1;
          this.wickets += 1;
          for (let i = 0; i < this.temp_batsman.length; i++) {
            if (this.current_batsman_id == this.temp_batsman[i].id) {
              console.log(this.current_batsman_id);
              if (this.current_batsman_id == this.batsman_1_id) {
                this.batsman_1_id = null;
                this.batsman_1_name = "";
                this.batsman_1_balls = 0;
                this.batsman_1_runs = 0;
                this.current_batsman_id = null;
              } else {
                this.batsman_2_id = null;
                this.batsman_2_name = "";
                this.batsman_2_balls = 0;
                this.batsman_2_runs = 0;
              }
              this.temp_batsman.splice(i, 1);
            }
          }
          break;
        }
        case 'O': {
          this.player_wicket('Run-Out');
          break;
        }
        case '0': {
          if (this.on_strike) {
            this.batsman_1_balls += 1;
          }
          else {
            this.batsman_2_balls += 1;
          }
          this.current_bowler_runs += 0;
          break;
        }
        case '1': {
          this.run_validate(1);
          break;
        }
        case '2': {
          this.run_validate(2);
          break;
        }
        case '3': {
          this.run_validate(3);
          
          break;
        }
        case '4': {
          this.run_validate(4);
          break;
        }
        case '5': {
          this.run_validate(5);
          break;
        }
        case '6': {
          this.run_validate(6);
          break;
        }
      }
    }
    if (this.next_ball) {
      this.overs += 0.1;
      console.log(this.overs);
      this.overs = +this.overs.toFixed(1);
      console.log(this.overs);      
      this.current_bowler_over += 0.1;
      this.current_bowler_over = +this.current_bowler_over.toFixed(1);
    }
    this.bowler_update();
    this.current_ball_score = "";
    this.run_entered = 0;
    this.extra_entered = 0;
    // if(this.temp_batsman.length == 1){this.result = "completed";}
    this.match_finish();
    // this.innings_finish();
  }

  /**
   * 
   * @param value 
   * Each Run Validation is given in this Function
   */
  run_validate(value) {
    this.score += value;
    this.current_bowler_runs += value;
    if (this.extra_run != true) {
      if (this.on_strike) {
        this.batsman_1_runs += value;
        if (this.current_ball_score.indexOf('n') == -1) {
          this.batsman_1_balls += 1;
        }
      }
      else {
        this.batsman_2_runs += value;
        if (this.current_ball_score.indexOf('n') == -1) {
          this.batsman_2_balls += 1;
        }
      }
    }
    if( this.current_ball_score.indexOf('l') != -1 || this.current_ball_score.indexOf('b') != -1){
      (this.on_strike)?this.batsman_1_balls += 1 :this.batsman_2_balls += 1;
    }
    (value == 1 || value == 3) ? this.batsman_swap() : "";
    this.batsman_update();
  }

  /**
   * @function
   * Batsman Swap To the Crease
   */
  batsman_swap() {
    if (this.on_strike) {
      this.current_batsman_id = this.batsman_2_id;
      this.current_batsman_name = this.batsman_2_name;
      this.current_batsman_runs = this.batsman_2_runs;
      this.current_batsman_balls = this.batsman_2_balls;
      this.on_strike = false;
    }
    else {
      this.current_batsman_id = this.batsman_1_id;
      this.current_batsman_name = this.batsman_1_name;
      this.current_batsman_runs = this.batsman_1_runs;
      this.current_batsman_balls = this.batsman_1_balls;
      this.on_strike = true;
    }
  }

  batsman_update() {
    if (this.on_strike) {
      this.current_batsman_id = this.batsman_1_id;
      this.current_batsman_name = this.batsman_1_name;
      this.current_batsman_runs = this.batsman_1_runs;
      this.current_batsman_balls = this.batsman_1_balls;
    }
    else {
      this.current_batsman_id = this.batsman_2_id;
      this.current_batsman_name = this.batsman_2_name;
      this.current_batsman_runs = this.batsman_2_runs;
      this.current_batsman_balls = this.batsman_2_balls;
    }
  }

  /**
   * @function
   * Bowler Score updates
   */
  bowler_update() {
    this.over.push(this.current_ball_score);
    let temp_overs = this.overs * 10;
    var temp_current = this.current_bowler_over * 10;
    if ((temp_overs % 10) == 6) {
      temp_overs = temp_overs / 10;
      this.overs = temp_overs + 1;
      this.overs = this.overs % 10;
      this.overs -= 0.6;
      this.overs = Math.round(this.overs);
      temp_current = temp_current / 10;
      this.current_bowler_over = temp_current + 1;
      this.current_bowler_over = this.current_bowler_over % 10;
      this.current_bowler_over -= 0.6;
      this.current_bowler_over = Math.round(this.current_bowler_over);
      let overs_length = this.over.length;
      this.storage.encryptandStore("over_" + this.overs, this.over);
      this.over.splice(0, overs_length);
      this.storing_values();
      this.prev_bowler_id=this.bowler_id;
      this.bowler_id = null;
      this.current_bowler_over = 0;
      this.current_bowler_runs = 0;
      this.current_bowler_wicket = 0;
      this.batsman_swap();
    }
      this.innings_finish();
  }
/**
 * @description This function Checks Whether the innnigs is Completed
 */
  innings_finish(){
    if (this.overs == this.match_details.overs || this.temp_batsman.length == 1) {
    if (this.batting_team == this.match_details.team_A_name) {
      let serve = this.utils.getApiConfigs("scoreboard_put");
      let pay: any;
      if (this.innings == "first") {
        let value = 0;
        pay = {
          "match_id": this.match_details.match_id,
          "team_id": this.match_details.team_A_id,
          "team_name": this.match_details.team_A_name,
          "score": this.score,
          "wicket": this.wickets,
          "status": "finished",
          "flag": value
        };
      } else {
        this.result = "completed";
        let value = 1;
        pay = {
          "match_id": this.match_details.match_id,
          "team_2_id": this.match_details.team_A_id,
          "team_2_name": this.match_details.team_A_name,
          "score_two": this.score,
          "wicket_two": this.wickets,
          "finalstatus": "finished",
          "flag": value
        };
      }
      return new Promise((resolve) => {
        this.service.invokeService(serve[0].method, serve[0].path, pay).then((respData: any) => {
          if (respData.code == 200) {
            if (this.innings == "first") {
              this.dialogues.showAlertPopup("Scoreboard", "First Innnings has Completed. Target for " + this.bowling_team + " is :" + (this.score + 1));
            }
            this.navCtrl.setRoot("HomePage");
            this.storage.removeItem('complete_score');
          }
        });
      });
    }
    else {
      let serve = this.utils.getApiConfigs("scoreboard_put");
      let pay: any;
      if (this.innings == "first") {
        let value = 0;
        pay = {
          "match_id": this.match_details.match_id,
          "team_id": this.match_details.team_B_id,
          "team_name": this.match_details.team_B_name,
          "score": this.score,
          "wicket": this.wickets,
          "status": "finished",
          "flag": value
        };
      } else {
        this.result = "completed";
        let value = 1;
        pay = {
          "match_id": this.match_details.match_id,
          "team_2_id": this.match_details.team_B_id,
          "team_2_name": this.match_details.team_B_name,
          "score_two": this.score,
          "wicket_two": this.wickets,
          "finalstatus": "finished",
          "flag": value
        };
      }
      return new Promise((resolve) => {
        this.service.invokeService(serve[0].method, serve[0].path, pay).then((respData: any) => {
          if (respData.code == 200) {
            if (this.innings == "first") {
              this.dialogues.showAlertPopup("Scoreboard", "First Innnings has Completed. Target for " + this.bowling_team + " is :" + (this.score + 1));
            }
            this.navCtrl.setRoot("HomePage");
            this.storage.removeItem('complete_score');
          }
        });
      });
    }
  }
  }

  /**
   * @description This Function Handles the result of the Match
   */
  match_finish() {
    let match_data: any;
    if (this.innings == "second") {
      let serve = this.utils.getApiConfigs("scoreboard_get");
      let pay: any = {
        "match_id": this.match_details.match_id
      };
      this.service.invokeService(serve[0].method, serve[0].path, pay).then((respData: any) => {
        if (respData.code == 200) {
          match_data = respData.payload[0];
          if ((this.score > match_data.score) || (this.result == "completed") || (this.temp_batsman.length == 1)) {
            if (this.score > match_data.score) {
              this.dialogues.showAlertPopup("Match Completed ", this.batting_team + " Won by " + this.temp_batsman.length + " Wickets");
              this.final_result = this.batting_team + " Won by " + this.temp_batsman.length + "Wickets";
            } else {
              if (this.score == match_data.score) {
                this.dialogues.showAlertPopup("Match Completed", "Match is draw");
                this.final_result = "Match is draw";
              }
              if (this.score < match_data.score) {
                this.dialogues.showAlertPopup("Match Completed ", this.bowling_team + " Won by " + (match_data.score - this.score) + " Runs");
                this.final_result = this.bowling_team + " Won by" + (match_data.score - this.score) + " Runs";
              }
            }
            this.match_update(this.match_details.match_id, this.final_result);
          }
        }
      });
    }
  }

/**
 * 
 * @param id Match_id
 * @param result Result of the match
 * @description This Function makes Api Call to Store the Result of the match
 */
  match_update(id, result) {
    let serve: any = this.utils.getApiConfigs("matchcomplete");
    let pay: any = {
      "match_id": id,
      "status": "Completed",
      "result": result
    };
    this.service.invokeService(serve[0].method, serve[0].path, pay).then((matchDetails: any) => {
      if (matchDetails.code == 200) {
        console.log(matchDetails);
      }
    });
    this.storage.removeItem('match_details');
    this.navCtrl.setRoot("HomePage");
  }

  /**
   * @param player_name
   * Choosing the Batsman 1
   */
  batsman_1_change(player_id) {
    if (this.batsman_1_name == "") {
      // if (player_id != this.batsman_2_id) {
        for (let i = 0; i < this.temp_batsman.length; i++) {
          if (player_id == this.temp_batsman[i].id) {
            this.batsman_1_name = this.temp_batsman[i].player;
            this.batsman_1_runs = this.temp_batsman[i].run;
            this.batsman_1_balls = this.temp_batsman[i].balls;
            this.temp_1 = player_id;
              console.log(this.temp_1);
          }
          if (this.on_strike) {
            this.current_batsman_id = player_id;
          }
        }
        if(this.batsman_2_id != null)
        {
          this.strike_player();
        }
      // } else {
      //   this.dialogues.showAlertPopup("Select Batsman", "Player is Already Selected");
      //   console.log(this.temp_1);
      //   // (this.temp_1 == undefined) ? this.batsman_1_id = null : this.batsman_1_id = this.temp_1;
      //   this.batsman_1_id = 0;
      //   this.batsman_1_name = "";
      // }
    } else {
      if(this.batsman_1_runs !=0 && this.batsman_1_balls!=0){
      this.dialogues.showAlertPopup("Batsman", "Player can't be changed now");
      console.log(this.temp_1);
      for (var i = 0; i < this.temp_batsman.length; i++) {
        if (this.temp_1 == this.temp_batsman[i].id) {
          this.batsman_1_id = this.temp_batsman[i].id;
          this.batsman_1_name =  this.temp_batsman[i].player;
          console.log(this.batsman_1_id);
        }
      }
    }
    else{
      if (player_id != this.batsman_2_id) {

        for (let i = 0; i < this.temp_batsman.length; i++) {
          // if (player_id == this.temp_batsman[i].id) {
            this.batsman_1_id = this.temp_batsman[i].id;
            this.batsman_1_name = this.temp_batsman[i].player;
            this.batsman_1_runs = this.temp_batsman[i].run;
            this.batsman_1_balls = this.temp_batsman[i].balls;
            this.temp_1 = player_id;
          }
          if (this.on_strike) {
            this.current_batsman_id = player_id;
          }
        }
        if(this.batsman_2_id != null)
        {
          this.strike_player();
        }
      // } else {
      //   this.dialogues.showAlertPopup("Select Batsman", "Player is Already Selected");
      //   // (this.temp_1 == undefined) ? this.batsman_1_id = 0 : this.batsman_1_id = this.temp_1;
      //   if(this.temp_1 != undefined){
      //     for (let i = 0; i < this.temp_batsman.length; i++) {
      //       if (this.temp_1 == this.temp_batsman[i].id) {
      //         this.batsman_1_id = this.temp_batsman[i].id;
      //         this.batsman_1_name = this.temp_batsman[i].player;
      //         this.batsman_1_runs = this.temp_batsman[i].run;
      //         this.batsman_1_balls = this.temp_batsman[i].balls;
      //         console.log(this.temp_1);
      //       }
      //       if (this.on_strike) {
      //         this.current_batsman_id = player_id;
      //       }
      //     }
      //   }
      // }
    }
  }
}

  /**
   * 
   * @param player_name 
   * Choosing the Batsman 2
   */
  batsman_2_change(player_id) {
    console.log(this.batsman_2_name);
    console.log(this.batsman_2_id);
    if (this.batsman_2_name == "") {
      // if (player_id != this.batsman_1_id) {
        for (var i = 0; i < this.temp_batsman.length; i++) {
          if (player_id == this.temp_batsman[i].id) {
            this.batsman_2_id = this.temp_batsman[i].id;
            this.batsman_2_name = this.temp_batsman[i].player;
            this.batsman_2_runs = this.temp_batsman[i].run;
            this.batsman_2_balls = this.temp_batsman[i].balls;
            this.temp_2 = player_id;
          }
          if (!this.on_strike) {
            this.current_batsman_id = player_id;
          }
        }
        if(this.batsman_1_id != null)
        {
          this.strike_player();
        }
      // } else {
      //   this.dialogues.showAlertPopup("Select Batsman", "Player is Already Selected1");
      //   console.log(this.temp_2);
      //   // (this.temp_2 == undefined) ? this.batsman_2_id = 0 : this.batsman_2_id = this.temp_2;
      //   this.batsman_2_id = 0;
      //   // this.batsman_2_name = "";
      // }
    }
    else {
      if(this.batsman_2_runs !=0 && this.batsman_2_balls!=0){
        console.log(this.temp_2);
      this.dialogues.showAlertPopup("Batsman", "Player can't be changed now");
      for (var i = 0; i < this.temp_batsman.length; i++) {
        if (this.temp_2 == this.temp_batsman[i].id) {
          this.batsman_2_id = this.temp_batsman[i].id;
          this.batsman_2_name =  this.temp_batsman[i].player;
          }
        }
      }
      else{
        // if (player_id != this.batsman_1_id) {
          
          for (var i = 0; i < this.temp_batsman.length; i++) {
            if (player_id == this.temp_batsman[i].id) {
              this.batsman_2_id = this.temp_batsman[i].id;
              this.batsman_2_name = this.temp_batsman[i].player;
              this.batsman_2_runs = this.temp_batsman[i].run;
              this.batsman_2_balls = this.temp_batsman[i].balls;
              this.temp_2 = player_id;
              console.log(this.temp_2);
            }
            if (!this.on_strike) {
              this.current_batsman_id = player_id;
            }
          }
          if(this.batsman_1_id != null)
        {
          this.strike_player();
        }
        // } else {
        //   this.dialogues.showAlertPopup("Select Batsman", "Player is Already Selected");
        //   // (this.temp_2 == undefined) ? this.batsman_2_id = 0 : this.batsman_2_id = this.temp_2;
        //   if(this.temp_2 != undefined){
        //     for (let i = 0; i < this.temp_batsman.length; i++) {
        //       if (this.temp_2 == this.temp_batsman[i].id) {
        //         this.batsman_2_id = this.temp_batsman[i].id;
        //         this.batsman_2_name = this.temp_batsman[i].player;
        //         this.batsman_2_runs = this.temp_batsman[i].run;
        //         this.batsman_2_balls = this.temp_batsman[i].balls;
        //         console.log(this.temp_2);
        //       }
        //       if (this.on_strike) {
        //         this.current_batsman_id = player_id;
        //       }
        //     }
        //   }else{
        //     this.batsman_2_id = 0;
        //     this.batsman_2_name = "";
        //   }
        // }
      }
    }    
  }

  /**
   * 
   * @param name 
   * Choosing the Bowler
   */
  bowler_choose(id) {
    if (this.overs != 0) {
      // if (this.prev_bowler_id != id) {
        let player_over = this.current_bowler_over * 10;
        if((player_over%10) == 0){
        for (var i = 0; i < this.temp_bowler.length; i++) {
          if (id == this.temp_bowler[i].id) {
            this.bowler_id = id;
            this.bowler_name = this.temp_bowler[i].player;
            this.current_bowler_over = this.temp_bowler[i].over;
            this.current_bowler_runs = this.temp_bowler[i].run;
            this.current_bowler_wicket = this.temp_bowler[i].wicket;
            // this.prev_bowler_id = id;
            // this.temp_3 = id;
          }
        }
      }else{
        this.dialogues.showAlertPopup("Select bowler", "Player can't be changed now");
        (this.temp_3== undefined) ? this.bowler_id = null : this.bowler_id = this.temp_3;
      }
      // }
      // else {
      //   this.dialogues.showAlertPopup("Select Another Player", "Player has Bowled the last Over. Please Select another bowler");
      //   (this.temp_3 == undefined) ? this.bowler_id = null : this.bowler_id = this.temp_3;
      // }
    }
    if (this.overs == 0) {
      for (var i = 0; i < this.temp_bowler.length; i++) {
        if (id == this.temp_bowler[i].id) {
          this.bowler_id = id;
          this.bowler_name = this.temp_bowler[i].player;
          this.current_bowler_over = this.temp_bowler[i].over;
          this.current_bowler_runs = this.temp_bowler[i].run;
          this.current_bowler_wicket = this.temp_bowler[i].wicket;
          // this.prev_bowler_id = id;
          // this.temp_3 = id;
        }
      }
    }
  }

  /**
   * 
   * @param text_value 
   * Retired hurt and Run out of player is validated in this function
   */
  player_wicket(text_value) {
    console.log(text_value);
    let alert = this.alertCtrl.create();
    alert.setTitle('Which Player is ' + text_value + '?');
    alert.addInput({
      type: 'radio',
      label: this.batsman_1_name,
      value: 'one'
    });
    alert.addInput({
      type: 'radio',
      label: this.batsman_2_name,
      value: 'two'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: (data: any) => {
        if (data == 'one') {
          this.retired_batsman = this.batsman_1_name;
          for (let i = 0; i < this.temp_batsman.length; i++) {
            if (text_value != "Retired-Hut" && this.temp_batsman[i].id == this.batsman_1_id) {
              this.temp_batsman.splice(i, 1);
              this.wickets += 1;
              this.batsman_1_name = "";
              this.batsman_1_id = null;
            }
          }
          this.batsman_1_balls = 0;
          this.batsman_1_runs = 0;
          this.batsman_1_name = "";
          this.batsman_1_id = null;
        } else {
          this.retired_batsman = this.batsman_2_name;
          for (let i = 0; i < this.temp_batsman.length; i++) {
            if (text_value != "Retired-Hut" && this.temp_batsman[i].id == this.batsman_2_id) {
              this.temp_batsman.splice(i, 1);
              this.wickets += 1;
              this.batsman_2_name = "";
              this.batsman_2_id = null;
            }
          }
          this.batsman_2_balls = 0;
          this.batsman_2_runs = 0;
          this.batsman_2_name = "";
          this.batsman_2_id = null;
        }
      }
    });
    alert.present();
  }

  strike_player(){
    let alert = this.alertCtrl.create();
    alert.setTitle('Which Player is on Strike?');
    alert.addInput({
      type: 'radio',
      label: this.batsman_1_name,
      value: 'one'
    });
    alert.addInput({
      type: 'radio',
      label: this.batsman_2_name,
      value: 'two'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: (data: any) => {
        if (data == 'one') {
          this.on_strike=true;
          this.batsman_update();
        }else{
          this.on_strike=false;
          this.batsman_update();
        }
      }
      });
        alert.present();
  }
}