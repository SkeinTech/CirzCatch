import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ThemeProvider } from '../../providers/theme/theme';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CommondialoguesProvider } from '../../providers/commondialogues/commondialogues';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  contactus:any='hide';
  selectedTheme: String;
  activeTheme:any="";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public dialogues:CommondialoguesProvider,
    public share:SocialSharing,
    public mytheme: ThemeProvider,) {
      this.mytheme.getActiveTheme().subscribe((val) => {
        this.selectedTheme = val;
        this.activeTheme = (this.selectedTheme === 'dark-theme') ? true : false;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  /**
   * Theme Changing
  */
  toggleAppTheme() {
    (this.selectedTheme === 'dark-theme') ? this.mytheme.setActiveTheme('light') : this.mytheme.setActiveTheme('dark-theme');
    this.activeTheme = (this.selectedTheme === 'dark-theme') ? true : false;
  }
  /**
   *Method for About us code 
  */
  aboutUs(){
    window.open("https://www.skeintech.com/", '_system');
  }

  /** 
   * Method for contact us
  */
  contactUs(){
    this.contactus = 'show';
  }

  shareEmail(){
    // Check if sharing via email is supported
    this.share.canShareViaEmail().then(() => {
      // Share via email
      this.share.shareViaEmail('', 'Crizcatch Enquires', ['enquiry@skeintech.com']).then(() => {
        console.log("Sending mail success");
      }).catch(() => {
        console.log("Sending mail error");
      });
    }).catch(() => {
      this.dialogues.showAlertPopup("Information","Email Sending feature is currently not available in your device,Kindly post your queries to enquiry@skeintech.com.")
    });
  }

}
