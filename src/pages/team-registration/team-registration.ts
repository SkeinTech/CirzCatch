import { CommonutilsProvider } from './../../providers/commonutils/commonutils';
import { CommonservicesProvider } from './../../providers/commonservices/commonservices';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CommondialoguesProvider } from '../../providers/commondialogues/commondialogues';
import { ImageViewerController } from 'ionic-img-viewer';
import { ChangeDetectorRef } from '@angular/core';
import { CommonstorageProvider } from '../../providers/commonstorage/commonstorage';
import { FabContainer } from 'ionic-angular';

/**Team Registration and Player Registration and View Team Players
 * @author Sathishkumar
 * @date 19.02.2018
*/
@IonicPage()
@Component({
  selector: 'page-team-registration',
  templateUrl: 'team-registration.html',
})
export class TeamRegistrationPage {
  login: any;
  loginData: any;
  totalFlag: any;
  playerLength: any;
  length: any;
  image: any;
  frompage: any = "";
  editPlayer: any = "";
  base64Image: any;
  teamID: FormGroup;
  teamRegister: FormGroup;
  teamDetails: string = '';
  teamData: any;
  playersList: any = "";
  data: any = "hide";
  imgModal: any = "";
  msg: any = "";
  defaultImg: any = "";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public service: CommonservicesProvider,
    public camera: Camera,
    public utils: CommonutilsProvider,
    public dialogue: CommondialoguesProvider,
    public modal: ModalController,
    public imageViewerCtrl: ImageViewerController,
    public detect: ChangeDetectorRef,
    public storage: CommonstorageProvider) {
    this.teamDetails = "team";
    this.playersList = "";
    this.msg = this.utils.getMessageConfigs("requestMessage");
    this.imgModal = this.utils.base64Data;
    this.defaultImg = this.utils.base64Data;
    /**Validation for Team Registration */
    this.teamID = this.formBuilder.group({
      team_id: [''],
      user_id: [''],
      team_name: ['', Validators.compose([Validators.pattern(/^\S+[A-Z]*[a-z]*\s?\S+$/),Validators.minLength(3),Validators.required])]
    });
    /**Validation for Player Registration */
    this.teamRegister = this.formBuilder.group({
      player_id: [''],
      team_id: ['', Validators.compose([Validators.required])],
      team_name: ['', Validators.compose([Validators.required])],
      first_name: ['', Validators.compose([Validators.pattern(/^\S+[A-Z]*[a-z]*\s?\S+$/),Validators.required])],
      last_name: ['', Validators.compose([Validators.pattern(/^\S+[A-Z]*[a-z]*\S*$/),Validators.required])],
      age: ['', Validators.compose([Validators.required])],
      role: ['', Validators.compose([Validators.required])]
    });
    this.frompage = this.navParams.get("frompage");
    this.editPlayer = this.navParams.get("playerListData");
  }
  /** Used to Generating Random Player ID*/
  randomID() {
    if (this.frompage != "playerlist") {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      this.teamRegister.controls["player_id"].setValue(text);
    }
  }
  /** Used to Generating Random Team ID*/
  randomTeamID() {
    if (this.frompage != "playerlist") {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      console.log(text);
      this.teamID.controls["team_id"].setValue(text);
    }
  }
  ionViewCanEnter() {
    this.defaultImg = this.utils.base64Data;
    this.userID();
    if (this.teamDetails == "reg") {
      this.teamRegister.reset();
    }
  }

  /**Getting User ID from Local Storage */
  userID() {
    return new Promise((resolve) => {
      this.storage.decryptandRead("loginData").then((respData: any) => {
        this.login = JSON.parse(respData);
        this.loginData = (this.login) ? this.login[0] : "";
        (this.loginData == "") ? this.navCtrl.setRoot("RegistrationPage") : "";
        resolve(this.loginData);
        this.getTeamDetail();
        this.editPlr();
      });
    });
  }
  /**Edit Player Details */
  editPlr() {
    if (this.frompage == "playerlist") {
      this.teamDetails = "reg";
      console.log("editplr", this.editPlayer);
      this.teamRegister.controls["player_id"].setValue(this.editPlayer.player_id);
      this.teamRegister.controls["team_name"].setValue(this.editPlayer.team_name);
      this.teamRegister.controls["team_id"].setValue(this.editPlayer.team_id);
      this.teamRegister.controls["first_name"].setValue(this.editPlayer.first_name);
      this.teamRegister.controls["last_name"].setValue(this.editPlayer.last_name);
      this.teamRegister.controls["age"].setValue(this.editPlayer.age);
      console.log(this.teamRegister);
      this.teamRegister.controls["role"].setValue(this.editPlayer.role);
      this.imgModal = this.editPlayer.image;
    }
  }
  /**Team Registration POST Method*/
  teamName() {
    this.randomTeamID();
    this.teamID.controls["user_id"].setValue(this.loginData.user_id);
    console.log(this.teamID.value);
    let servicePath = this.utils.getApiConfigs("postTeam");
    console.log(servicePath);
    this.service.invokeService(servicePath[0].method, servicePath[0].path, this.teamID.value).then((respData: any) => {
      console.log(respData);
      if (respData.code == 200) {
        this.dialogue.showToastPopup(respData.message, 4000, "top");
        console.log(respData);
        this.teamID.reset();
        this.teamDetails = "reg";
        this.getTeamDetail();
      }else{
        this.dialogue.showToastPopup(respData.message, 4000, "top");
      }
    });
  }
  /** Get Team Name and Team ID  GET Method*/
  getTeamDetail() {
    let servicePath = this.utils.getApiConfigs("getTeam");
    console.log(servicePath);
    return new Promise((resolve) => {
      let payload: any = { "user_id": this.loginData.user_id };
      this.service.invokeService(servicePath[0].method, servicePath[0].path, payload).then((respData: any) => {
        console.log(respData);
        if (respData.code == 200) {
          console.log(respData);
          this.teamData = respData.payload;
          console.log(this.teamData);
        }
      });
    });
  }
  /**Getting Team ID when select the team */
  selectTeam(data: any) {
    for (var i = 0; i < this.teamData.length; i++) {
      if (data == this.teamData[i].team_name) {
        this.teamRegister.controls['team_id'].setValue(this.teamData[i].team_id);
        this.totalPlayers(this.teamData[i]);
        this.totalFlag = "teamSelected";
        this.randomID();
      }
    }
    console.log(data);
  }
  /** Player Registration POST Method*/
  addToTeam() {
    let servicePath = (this.frompage == "playerlist") ? this.utils.getApiConfigs("teamPut") : this.utils.getApiConfigs("teamReg");
    console.log(servicePath);
    let control: FormControl = new FormControl(this.imgModal);
    this.teamRegister.addControl("image", control);
    this.dialogue.showLoading(this.msg[0].message);
    this.service.invokeService(servicePath[0].method, servicePath[0].path, this.teamRegister.value).then((respData: any) => {
      this.dialogue.dismissLoader();
      if (respData.code == 200) {
        this.dialogue.showAlertPopup("Success", "Player Details Added Successfully");
        this.teamDetails = "list";
        // console.log("before Resettinggg",this.teamRegister.value);
        this.teamRegister.reset();
        // console.log("after Resettinggg",this.teamRegister.value);
         this.teamRegister.controls["image"].setValue(this.defaultImg);
        //  console.log("default image",this.teamRegister.value);

       // this.editPlayer = "";
        this.length = null;
      } else if (respData.status == 200) {
        this.dialogue.showAlertPopup("Success", "Player Details Updated Successfully");
        this.teamID.controls["team_name"].setValue("");
        this.teamRegister.reset();
        this.teamRegister.controls["image"].patchValue(this.defaultImg);
        this.frompage = "updated";
        //this.toggleSection();
        //this.navCtrl.pop();
      } else {
        console.log(respData);
      }
    });
  }
  ngAfterViewChecked() {
    this.detect.detectChanges();
  }
  /**Getting Team with Players */
  toggleSection(datav: any) {
    let servicePath = this.utils.getApiConfigs("teamGet");
    let payload: any = { "team_id": datav.team_id, "team_name": datav.team_name };
    this.dialogue.showLoading(this.msg[0].message);
    this.service.invokeService(servicePath[0].method, servicePath[0].path, payload).then((respVal: any) => {
      this.dialogue.dismissLoader();
      console.log("console log", respVal);
      this.playersList = respVal.payload;
      console.log("PlayerList", this.playersList);
      let mdc = this.modal.create("PlayerlistPage", { "playerList": this.playersList, "team_Name": datav.team_name });
      mdc.present();
    });
  }
  /**Total Players */
  totalPlayers(data: any) {
    let servicePath = this.utils.getApiConfigs("teamGet");
    let payload: any = { "team_id": data.team_id, "team_name": data.team_name };
    this.service.invokeService(servicePath[0].method, servicePath[0].path, payload).then((respVal: any) => {
      this.playerLength = respVal.payload;
      (this.playerLength == null) ? this.length = 0 : this.length = this.playerLength.length;
    });
  }

  /**Set Player Image */
  openCamera(fab?: FabContainer) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true
    }
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.imgModal = this.base64Image;
      if (fab !== undefined) {
        fab.close();
      }
    }, (err) => {
      console.log(err);
    });
  }
  openGallery(fab?: FabContainer) {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL, quality: 50,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.imgModal = this.base64Image;
      if (fab !== undefined) {
        fab.close();
      }
    }, (err) => {
      console.log(err);
    });
  }
  /** Touch to View Player Image */
  onClick(imageToView) {
    const viewer = this.imageViewerCtrl.create(imageToView)
    viewer.present();
  }
}