import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamRegistrationPage } from './team-registration';

@NgModule({
  declarations: [
    TeamRegistrationPage,
  ],
  imports: [
    IonicPageModule.forChild(TeamRegistrationPage),
  ],
})
export class TeamRegistrationPageModule {}
