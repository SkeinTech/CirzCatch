import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Screenshot } from '@ionic-native/screenshot';
import { DatePipe } from '@angular/common';
import { CommonservicesProvider } from '../../providers/commonservices/commonservices';
import { CommonutilsProvider } from '../../providers/commonutils/commonutils';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { CommondialoguesProvider } from '../../providers/commondialogues/commondialogues';
import { CommonstorageProvider } from '../../providers/commonstorage/commonstorage';


/**
 * @author Karthikeyan.K
 * @description Match Details Page Component
 * @date 14 February 2018
 */

@IonicPage()
@Component({
  selector: 'page-match-details',
  templateUrl: 'match-details.html',
})
export class MatchDetailsPage {
  loginData:any;
  login:any;
  team_A:any;
  team_B:any;  
  addmatch:any = 'hide';
  matchDet:any = "";
  matchForm: FormGroup;
  teamData:any="";
  select_B_name:any;
  team_A_length:any;
  team_B_length:any;
  B_name:string ="";
  A_name:string ="";  

  constructor(public navCtrl: NavController,
    private socialSharing: SocialSharing,
    private formBuilder: FormBuilder,
    public navParams: NavParams,
    public service:CommonservicesProvider,
    public utils:CommonutilsProvider,
    public dialogues:CommondialoguesProvider,
    private screenshot: Screenshot,
    public storage: CommonstorageProvider,
    private datePipe: DatePipe) {
    this.matchForm = this.formBuilder.group({
      user_id:[''],
      match_id:['',Validators.compose([Validators.required])],
      team_A_id:['', Validators.compose([Validators.required])],
      team_B_id:['', Validators.compose([Validators.required])],
      team_A_name: ['', Validators.compose([Validators.required])],
      team_B_name: ['', Validators.compose([Validators.required])],
      overs:['',Validators.compose([Validators.required])],
      match_date:['',Validators.compose([Validators.required])]
      // match_time:['',Validators.compose([Validators.required])]
    });
    this.teamData = "";
    this.matchDet = "";
  }

  ionViewCanEnter(){
    return new Promise((resolve) => {
      this.storage.decryptandRead("loginData").then((respData: any) => {
        this.login = JSON.parse(respData);
        this.loginData = (this.login) ? this.login[0] : "";
        (this.loginData == "") ? this.navCtrl.setRoot("RegistrationPage") : "";
        resolve(this.loginData);
        this.getTeamDetail();
        this.getMatchDetails();
        this.makeid();
      });
    });
  }

  /**
   * 
   * @param value Match Status 
   * @description Checks the Match Status
   */
  navigatepage(value){
    let data:any = value;
    if(data.status == null){
      let today:number = Date.now();
      let selected_date:Date = new Date(data.match_date);
        if(this.datePipe.transform(today,'yyyy-MM-dd') == this.datePipe.transform(selected_date,'yyyy-MM-dd')){
          this.check_toss(data);
          }else{
            this.dialogues.showAlertPopup("Match Status","You Can't Start this Match Today");
          }
        }
    if(data.status == "Running"){
      this.dialogues.showAlertPopup("Match Status","Already the Match is Started");
    }
  }
  /**
   * method to get the number of players in the team
   * @param team_id
   * @param team_name
   * @param value
   */
  totalPlayers(team_id: any,team_name: any,value) {
    let servicePath = this.utils.getApiConfigs("teamGet");
    let payload: any = { "team_id": team_id, "team_name": team_name };
    this.service.invokeService(servicePath[0].method, servicePath[0].path, payload).then((respVal: any) => {
      console.log(respVal.payload);
      let Length = respVal.payload;
      if(Length != null){
        if(value == 'A'){
          this.team_A_length = Length.length
        }
        if(value == 'B'){
          this.team_B_length = Length.length;
        }
      }else{
      if(value == 'B'){
        this.team_B_length = 0;
      }
      if(value == 'A'){
        this.team_A_length = 0;
      }
      }
      console.log(this.team_B_length);
      console.log(this.team_A_length);      
    });
  }

  /**
   * 
   * @param value Match Details
   * @description Checks the Toss is Completed or not
   */
  check_toss(value) {
    let serve = this.utils.getApiConfigs("toss_get");   
    this.service.invokeService(serve[0].method, serve[0].path+value.match_id, '').then((respData: any) => {
      if (respData.code == 200) {
        if (respData.payload == null) {
          this.dialogues.showAlertPopup("Toss Info", "Please Begin Toss Using Match Id");
          this.navCtrl.push("TossPage");
        } else {
          this.navCtrl.push("ScoreboardPage",{"matchdet":value});
          this.match_update(value.match_id,"Running");
        }
      }
    });
  }
  
  /**
   * 
   * @param id Match_id
   * @param match_status Status of the Match
   * @description This Function updates the Match Details 
   */
  match_update(id,match_status){
    let serve:any = this.utils.getApiConfigs("matchput");
    let pay:any={
      "match_id":id,
      "status":match_status 
    };
    this.service.invokeService(serve[0].method,serve[0].path,pay).then((matchDetails:any)=>{
      if(matchDetails.code == 200){
      }
    });
  }
  /** 
   * Method for generating dynamic mstch id
  */
  makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++){
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    this.matchForm.get(["match_id"]).setValue(text);
  }
  /**
   * @description Sharing Match Detail through Whatsapp
   * @param values
   */
  sharing_to_whatsapp(values){
    this.screenshot.URI(80).then((success:any) =>{
          this.socialSharing.shareViaWhatsApp(values.value, success.URI);
          }).catch(() => {
          alert("WhatsApp Sharing Error");
    });
  }
  /**
   * @description Sharing Match Detail through E-Mail
   * @param values
   */
  //this.socialSharing.shareViaEmail(values.value, "CricCatchMatchInfo",[],[],[],success.URI)
  sharing_to_mail(values){
    this.screenshot.URI(80).then((success:any) =>{
      this.socialSharing.canShareViaEmail().then(() => {
        // Share via email
        this.socialSharing.shareViaEmail(values.value, "CricCatchMatchInfo", [],[],[],success.URI).then(() => {
          console.log("Sending mail success");
        }).catch(() => {
          console.log("Sending mail error");
        });
      }).catch(() => {
        this.dialogues.showAlertPopup("Information","Email Sending feature is currently not available in your device,Kindly post your queries to enquiry@skeintech.com.")
      });
      });
  }
  /**Getting Team ID when select the team */
  selectTeam(data: any,Val) {
    for (var i = 0; i < this.teamData.length; i++) {
      if (data.team_id == this.teamData[i].team_id) {
        (Val =='A') ? this.A_name = this.teamData[i].team_name: this.B_name = this.teamData[i].team_name;        
        if(this.matchForm.controls['team_B_name'].value != this.matchForm.controls['team_A_name'].value){
          (Val =='A') ? this.matchForm.controls['team_A_id'].setValue(this.teamData[i].team_id) : this.matchForm.controls['team_B_id'].setValue(this.teamData[i].team_id);
          (Val =='A') ?this.team_A = this.teamData[i].team_id:this.team_B = this.teamData[i].team_id;
          (Val =='A') ? this.totalPlayers(data.team_id,data.team_name,'A') : this.totalPlayers(data.team_id,data.team_name,'B');
        }else{
          this.dialogues.showAlertPopup("Select Match","Choose Different Teams");
          this.matchForm.controls['team_B_name'].setValue("");
        }
      }
    }
  }
  /**
   * Method for showing add match details
   * @param data
   */
  addmatchdetails(data){
    if(this.teamData.length >= 2){
    this.addmatch = (data == 'show') ? 'show' :'hide';
    }else{
      this.dialogues.showAlertPopup("Schedule Match","Register Atleast 2 Teams to Schedule a match");
      this.team_A = null;
      this.team_B = null;
      this.matchForm.controls['match_date'].setValue('');
    }
  }
  /** Get Team Name and Team ID  GET Method*/
  getTeamDetail() {
    let servicePath = this.utils.getApiConfigs("getTeam");
    let payload ={"user_id":this.loginData.user_id};
    this.service.invokeService(servicePath[0].method, servicePath[0].path, payload).then((respData: any) => {
      if (respData.code == 200) {
        this.teamData = respData.payload;
        console.log(this.teamData);
      }
    });
  }
  /**
   * Method for getting match details
  */
  getMatchDetails(){
    let serve:any = this.utils.getApiConfigs("matchGet");
    let payload:any = {"user_id":this.loginData.user_id};
    this.service.invokeService(serve[0].method,serve[0].path,payload ).then((matchDetails:any)=>{
      if(matchDetails){
        this.matchDet = matchDetails.payload;
      }
    });
  }
  /**
   * Method for adding new match details
  */
  addMatch(){
    this.matchForm.controls['user_id'].setValue(this.loginData.user_id);
    for (var i = 0; i < this.teamData.length; i++) {
      if (this.matchForm.controls['team_A_name'].value.team_id == this.teamData[i].team_id) {
        this.matchForm.controls['team_A_name'].setValue(this.teamData[i].team_name);
      }
      if(this.matchForm.controls['team_B_name'].value.team_id == this.teamData[i].team_id)
      {
        this.matchForm.controls['team_B_name'].setValue(this.teamData[i].team_name);
      }
    }
    let serve = this.utils.getApiConfigs("matchReg");
    let today:number = Date.now();
    let selected_date:Date = new Date(this.matchForm.controls['match_date'].value);
          if(this.team_A_length != 0 && this.team_B_length != 0 ){
            console.log("zero"+this.team_B_length);
      console.log("zero"+this.team_A_length);
              if((this.team_A_length == this.team_B_length) || (this.team_A_length >= 11 && this.team_B_length >=11)){
                console.log("equal"+this.team_B_length);
      console.log("equal"+this.team_A_length);
                if(this.datePipe.transform(today,'yyyy-MM-dd') <= this.datePipe.transform(selected_date,'yyyy-MM-dd')){
                  return new Promise((resolve) => {
                  this.service.invokeService(serve[0].method,serve[0].path,this.matchForm.value).then((matchData:any)=>{
                    if(matchData){
                      this.getMatchDetails();
                      this.matchForm.reset();
                      this.addmatch = 'hide';
                      }
                      });
                    });
                    }else{
                      this.dialogues.showAlertPopup("Change Date", "Please Enter the Valid Date");
                    }
                  }else{
                    this.dialogues.showAlertPopup("Players","Team Players Should be equal or more than 11 players");              
                }
              }else{
              this.dialogues.showAlertPopup("Players","Please Add Atleast two players in a Team");
              }
    
          
        
  }
}