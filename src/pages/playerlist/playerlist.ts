import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import { ImageViewerController } from 'ionic-img-viewer';
@IonicPage()
@Component({
  selector: 'page-playerlist',
  templateUrl: 'playerlist.html',
})
export class PlayerlistPage {
  playerListv:any=[];
  teamName:any ="";
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public view:ViewController,
    public imageViewerCtrl: ImageViewerController,
  ) {
    this.playerListv = this.navParams.get("playerList");
    this.teamName = this.navParams.get("team_Name");
    console.log("from nav params",this.playerListv);
    console.log("title",this.teamName);
  }
  /**
   * Method for closing the modal popup
  */
  closeMenu(){
    this.view.dismiss();
  }
/**
 * View PLayer Image
 */
  viewImage(imageToView) {
    const viewer = this.imageViewerCtrl.create(imageToView)
    viewer.present();
  }
  /**Edit Player */
  editPlayer(data:any){
    console.log(data);
    this.view.dismiss();
    this.navCtrl.push("TeamRegistrationPage",{"playerListData":data,"frompage":"playerlist"});
  }
}
