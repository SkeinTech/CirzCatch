import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TossPage } from './toss';

@NgModule({
  declarations: [
    TossPage,
  ],
  imports: [
    IonicPageModule.forChild(TossPage),
  ],
})
export class TossPageModule {}
