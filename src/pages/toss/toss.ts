import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonservicesProvider } from '../../providers/commonservices/commonservices';
import { CommonutilsProvider } from '../../providers/commonutils/commonutils';
import { CommondialoguesProvider } from '../../providers/commondialogues/commondialogues';
import { CommonstorageProvider } from '../../providers/commonstorage/commonstorage';

 /**
 * Toss module store the details of Selected Team for batting or bowling depends up on Toss choosen by the team. 
 * It's shows the Match date details and the team.
 * @author Suganyadevi
 * @date 20.02.2018
 */


@IonicPage()
@Component({
  selector: 'page-toss',
  templateUrl: 'toss.html',

})

export class TossPage {
  showmatchid:any;
  showmatch: any;
  loginData:any;
  login:any;
  saveDisabled: boolean;
  toss_detail: any;
  match_details: any;
  showBtn: any;
  showToss: any;
  buttonDisabled: boolean;
  today: Date;
  matchDet:any[]=[];
  matchID:any="";
  fialMatchDet:any="";
  teamname: any = "";
  choice:any = "";
  tossVal:any = "";
  show: any;
  disabled: boolean = true;
  showId: boolean;
  heads:number=0;
  tails:number=0;
  x:any;
  tossImgsrc:any="stop";
  tossChoiceObj: any = [
    {
      heads:"Head",
      tails:"Tail"
  }
  ];
  teamNameObj : any = [{
    teamA:"CSK",
    teamB:"KKR"
  }
  ];
  teamChoiceObj : any = [
    {
      choiceA:"Batting",
      choiceB:"Bowling"
    }
  ];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public utils:CommonutilsProvider,
    public service:CommonservicesProvider,
    public dialogues:CommondialoguesProvider,
    public storage: CommonstorageProvider) {
       this.today = new Date();
       this.fialMatchDet = "";
        this.show = !this.show;
        this.showBtn = !this.showBtn;
         
  }

  ionViewCanEnter(){
    this.showmatch = false;
    return new Promise((resolve) => {
      this.storage.decryptandRead("loginData").then((respData: any) => {
        this.login = JSON.parse(respData);
        this.loginData = (this.login) ? this.login[0] : "";
        (this.loginData == "") ? this.navCtrl.setRoot("RegistrationPage") : "";
        resolve(this.loginData);
        this.getMatchDetails();
      });
    });
  }
  getTeamDetails(data:any){
    this.check_toss(data).then((datab:any)=>{
      if(datab == null){
    console.log("matchid details",data);
    this.matchDet.filter((itemVal:any)=>{
      if(data == itemVal.match_id){
        //MATCH ID HIDE 
      this.showmatch = true;
      this.showmatchid = itemVal.match_id;
        this.fialMatchDet = itemVal;
      }
    });
      }
    });
     
     
  }
  /**
   * Method for getting match details
  */
  getMatchDetails(){
    let serve:any = this.utils.getApiConfigs("matchGet");
    let payload:any = {"user_id":this.loginData.user_id};
    this.service.invokeService(serve[0].method,serve[0].path,payload ).then((matchDetails:any)=>{
      if(matchDetails.payload != null)
      {
      for(let i=0;i<matchDetails.payload.length;i++)
      {
        this.check_toss(matchDetails.payload[i].match_id).then((datab:any)=>{
          if(datab == null){
            this.matchDet.push(matchDetails.payload[i])
          }
        });
      }
    }
    });
  }

  check_toss(value) {
    let serve = this.utils.getApiConfigs("toss_get");
    return new Promise((resolve:any)=>{
    this.service.invokeService(serve[0].method, serve[0].path + value, '').then((respData: any) => {
      resolve(respData.payload);
    });
    });
  }
  /**
   * Method for start and stop the toss
   * @param data 
   */

  tossStart(data:any){
    this.tossImgsrc = 'start';
    setTimeout(() => {
      this.fliptoss().then((tossData:any)=>{
        this.tossVal = (tossData)?"Head":"Tail";
        if(tossData){
          this.tossImgsrc = 'heads';
         }else{
           this.tossImgsrc = 'tails';
         }
      });
      this.show = !this.show;
      this.showToss = !this.showToss;
      this.showBtn = !this.showBtn;
      
    }, 4000);
    this.disabled = this.disabled ? false : true;
  }
  /**
   * Method for store the team,toss & choice values
   * @param selName 
   */
  selectRadio(selName: any,type){
    let choiceval:any="";
    (type=='team') ? this.teamname = selName : this.choice = selName;

    if(type == 'toss')
    {
      this.tossVal = selName;
    }
   
    (this.teamname != "" && this.tossVal != "") ? this.buttonDisabled=true : this.buttonDisabled=false;

    if(type == 'choice'){
      choiceval = selName;
    }
     // SAVE BUTTON VALIDATION
    (this.teamname !="" && choiceval !="") ? this.saveDisabled=true : this.saveDisabled=false;

  }
   /**
   * Method for disable and enable the radio buttons 
   */

  getDisabled() {
    return this.disabled;
  }
  /**
   * Method for show and hide the success result and radio button columns 
   */
  saveData(){
     this.showId = !this.showId;
     this.showBtn = !this.showBtn;
  }
  /**
   * Method for Back to home
   */
  backtohome()
  {
    this.dialogues.showAlertPopup("Toss Completed", "Proceed the Match now");
    this.navCtrl.setRoot('HomePage');
  }  
  /**
   * Method for flipping a toss
   */
  fliptoss(){
    return new Promise((resolve)=>{
      this.x = (Math.floor(Math.random() * 2) ===0);
      resolve(this.x);
    });
  }

  /**
   * Method for Toss registration & show the result from DB
   */
  registerToss(){
    let serve = this.utils.getApiConfigs("toss");
    let teamId = (this.teamname == this.fialMatchDet.team_A_name) ? this.fialMatchDet.team_A_id : this.fialMatchDet.team_B_id;
    let pay:any = {
    "team_id":teamId,
    "match_id":this.matchID,
    "team_name":this.teamname,
    "choose_toss":this.tossVal,
    "choice":this.choice
    };
    this.service.invokeService(serve[0].method,serve[0].path,pay).then((respData:any)=>{
      if(respData.code == 200){
        this.saveData();
      }
    });
  }
}
