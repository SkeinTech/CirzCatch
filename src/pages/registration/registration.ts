import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { CommonservicesProvider } from '../../providers/commonservices/commonservices';
import { CommonstorageProvider } from '../../providers/commonstorage/commonstorage';
import { CommondialoguesProvider } from '../../providers/commondialogues/commondialogues';
import { CommonutilsProvider } from './../../providers/commonutils/commonutils';
import { Events } from 'ionic-angular';


/**User Registration and User Login
 * @author Sathishkumar
 * @date 16.02.2018
 * @modified by @author Arulselvam
 * @modified Date 22.02.2018
*/

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {

  profilePage: any;
  userEdit: any;
  reg: any = "signIn";
  loginForm: FormGroup;
  registerForm: FormGroup;
  loginData: any = "";
  updateForm: FormGroup;
  msg: any = "";
  editUserDetails: any = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    private formBuilder: FormBuilder,
    private service: CommonservicesProvider,
    public dialogues: CommondialoguesProvider,
    public storage: CommonstorageProvider,
    public utils: CommonutilsProvider,
    public events: Events) {
    this.menu.swipeEnable(false);
    this.msg = this.utils.getMessageConfigs("requestMessage");
    /**Login Validation */
    this.loginForm = this.formBuilder.group({
      email_id: ['', Validators.compose([Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(12), Validators.required])],
    });
    /**Registration Validation */
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.minLength(3), Validators.maxLength(50), Validators.required])],
      email_id: ['', Validators.compose([Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(12), Validators.required])],
      mobile_no: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10), Validators.required])]
    });
    /**Login Validation */
    this.updateForm = this.formBuilder.group({
      email_id: ['', Validators.compose([Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(12), Validators.required])],
      cnfrmpassword: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(12), Validators.required])]
    });

    this.profilePage = this.navParams.get("profilePage");
    this.userEdit = this.navParams.get("editUserDetails");
  }
  /**
   * Method for doing action for sign in and sign up
   * based on param data, it will decide the operations to be done
   * @param data
   */
  register(data: any) {
    this.reg = (data == "signUp") ? "signIn" : "signUp";
  }
  /**
   * Method for sigining into the app
  */
  signIn() {
    let servicePath = this.utils.getApiConfigs("login");
    this.dialogues.showLoading(this.msg[0].message);
    this.service.invokeService(servicePath[0].method, servicePath[0].path, this.loginForm.value).then((respData: any) => {
      this.dialogues.dismissLoader();
      if (respData.code == 200) {
        this.loginData = respData.payload;
        this.storage.encryptandStore("loginData", this.loginData);
        this.navCtrl.setRoot('HomePage');
      } else {
        // this.dialogues.showToastPopup("Invalid Username or Password","4000","top");
        this.dialogues.showAlertPopup("Error", "Invalid Username or Password");
      }
    });
  }
  /**
   * Method for registering into the app
  */
  signUp() {
    let servicePath = (this.profilePage !== 'editUserDetails') ? this.utils.getApiConfigs("signup") : this.utils.getApiConfigs("signupPut");
    console.log("signup or edit", this.userEdit);
    this.dialogues.showLoading(this.msg[0].message);
    this.service.invokeService(servicePath[0].method, servicePath[0].path, this.registerForm.value).then((respData: any) => {
      this.dialogues.dismissLoader();
      console.log(respData);
      let userdata: any = "" || [];
      this.storage.removeItem("loginData");
      if (respData.code == 200) {
        this.dialogues.showToastPopup("Registered Successfully", 4000, "top");
        this.reg = 'signIn';
        this.registerForm.reset();
        console.log("signup or edit", this.userEdit);
      } else if (respData.status == 200) {
        console.log("status Response",respData);
        userdata.push(this.registerForm.value);
        this.storage.encryptandStore("loginData", userdata);
        this.dialogues.showToastPopup("Updated Successfully", 4000, "top");
        this.navCtrl.pop();
      } else {
        this.dialogues.showToastPopup("Registration failed", 4000, "top");
      }
    });
  }
  /**
   * Method for Showing update password container
  */
  openForPass() {
    this.reg = 'passwordUpdate';
    let emailVal = this.loginForm.controls["email_id"].value;
    this.updateForm.controls["email_id"].setValue(emailVal);
  }
  /**
   * Method for updating password
  */
  updatePass() {
    let servicePath = this.utils.getApiConfigs("passUpdate");
    let payload = {
      "email_id": this.updateForm.controls["email_id"].value,
      "password": this.updateForm.controls["cnfrmpassword"].value
    }
    /* Code for evaluating password and confirm password is matching */
    if (this.updateForm.controls["cnfrmpassword"].value != this.updateForm.controls["password"].value) {
      this.dialogues.showAlertPopup("Information", "Password not matches");
      this.updateForm.controls["cnfrmpassword"].setValue("");
      this.updateForm.controls["password"].setValue("");
    } else {
      this.dialogues.showLoading(this.msg[0].message);
      //Code for calling the service api
      this.service.invokeService(servicePath[0].method, servicePath[0].path, payload).then((respData: any) => {
        this.dialogues.dismissLoader();
        if (respData.code == 200) {
          this.dialogues.showToastPopup(respData.message, "3000", "bottom");
          this.reg = 'signIn';
        } else {
          this.dialogues.showToastPopup(respData.message, "4000", "top");
        }
      });
    }
  }
  /**Edit User Details */
  editUser() {
    if (this.profilePage == 'editUserDetails') {
      console.log("editUser", this.userEdit);
      this.reg = 'signUp';
      this.registerForm.controls['username'].setValue(this.userEdit.username);
      this.registerForm.controls['email_id'].setValue(this.userEdit.email_id);
      this.registerForm.controls['password'].setValue(this.userEdit.password);
      this.registerForm.controls['mobile_no'].setValue(this.userEdit.mobile_no);
    }
  }
  ionViewCanEnter() {
    this.utils.checkNetwork();
    this.editUser();
  }
}
