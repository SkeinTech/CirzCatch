import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonstorageProvider } from '../../providers/commonstorage/commonstorage';


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  loginData: any = "";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: CommonstorageProvider) {
  }
  /**
   * Method for reading the data  from storage and navigate process
   * Based on local storage data,navigating to login and home page
  */
  ionViewCanEnter() {
    this.storage.decryptandRead("loginData").then((respData: any) => {
      console.log("respdata" + respData);
      let login = JSON.parse(respData);
      this.loginData = (login) ? login[0] : "";
      console.log("Login data from local storage", this.loginData);
    });
  }
  /**
   * Method for reading the data  from storage and navigate process
   * Based on local storage data,navigating to login and home page
  */
  validateandLogin() {
    this.storage.decryptandRead("loginData").then((respData: any) => {
      let login = JSON.parse(respData);
      this.loginData = (login) ? login[0] : "";
      console.log("Login data from local storage", this.loginData);
    });
  }
  /**Edit User Details */
  editUser(data:any) {
    this.navCtrl.push("RegistrationPage",{"editUserDetails":data,"profilePage":"editUserDetails"});
  }

}
