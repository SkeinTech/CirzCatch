import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

/**
 * Common provider for setting/getting stored values within app
 * It's a storage layer for setting/getting stored values
 * Volatile phone memory(Internal/SD card) is used to store values
 * Values will be stored until the app gets uninstall from device
 * Values are encrypted/decrypted before storing & reading
 * @author ArulSelvam
 * @date 14.02.2018
 */
@Injectable()
export class CommonstorageProvider {

  constructor(
    public http: HttpClient,
    public storage:Storage) {
    console.log('CommonstorageProvider Provider');
  }
  /**
   * Method for storing data into local mobile SD/Internal Card
   * Values will be encrypted and stored before storing
   * @param key
   * @param data
   */
  encryptandStore(key,data){
    let dataVal = (typeof data == 'object') ? JSON.stringify(data) : data;
    return new  Promise((resolve)=>{
      //resolve(this.storage.set(key,this.security.encryptPayload(dataVal)));
      resolve(this.storage.set(key,dataVal));
    });
  }
  /**
   * Method for reading data from local mobile SD/Internal Card
   * Values will be decrypted and reading from storage
   * @param key
   */
  decryptandRead(key) {
    return new  Promise((resolve)=>{
      //let dataVal = this.storage.get(this.security.decryptPayload(key));
      let dataVal = this.storage.get(key);
      resolve(dataVal);
    });
  }
  /**
   * Method for removing the particular item from local storage
   * Based on key value the item will be removed
   * @param key
   */
  removeItem(key){
    return new Promise((resolve)=>{
      this.storage.remove(key);
      resolve(true);
    });
  }
  /**
   * Method for clearing the local storage data
   * Clear all the data from session storage
  */
  removeallData(){
    return new Promise((resolve)=>{
      this.storage.clear();
      resolve(true);
    });
  }
  /**
   * Method for storing data into session storage
   * Values will be encrypted and stored before storing
   * @param key
   * @param data
   */
  encryptSessionandStore(key,data){
    let dataVal:any = (typeof data == 'object') ? JSON.stringify(data) : data;
    //let encryptvalue = this.security.encryptPayload(dataVal);
    return new Promise((resolve)=>{
      sessionStorage.setItem(key,dataVal);
      resolve(true);
    });
  }
  /**
   * Method for reading data from session storage
   * Values will be decrypted and reading from storage
   * @param key
   */
  decryptSessionandRead(key) {
    return new  Promise((resolve)=>{
      let datavalue = sessionStorage.getItem(key);
      //this.security.decryptPayload;
      resolve(datavalue);
    });
  }
  /**
   * Method for removing the particular item from session storage
   * Based on key value the item will be removed
   * @param key 
   */
  removeSessionItem(key){
    return new Promise((resolve)=>{
      sessionStorage.removeItem(key);
      resolve(true);
    })
  }
  /** 
   * Method for clearing the session storage data
   * Clear all the data from session storage
  */
  removeallSessionData(){
    return new Promise((resolve)=>{
      sessionStorage.clear()
      resolve(true);
    });
  }
}
