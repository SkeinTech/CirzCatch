import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController, Platform } from 'ionic-angular';
import { CommonutilsProvider } from '../commonutils/commonutils';
/**
 * Common provider for showing toast,alerts,prompts into the app
 * It's a service layer for accessing from/to services from backend server
 * @author ArulSelvam
 * @date 14.02.2018
 */
@Injectable()
export class CommondialoguesProvider {
  ldcntrl:any = "";
  constructor(
    public http: HttpClient,
    public platform:Platform,
    public alertCtrl:AlertController,
    public toastCtrl:ToastController,
    public loading:LoadingController,
    public utils:CommonutilsProvider) {
    console.log('CommondialoguesProvider Provider');
  }
  /**
   * Method for shwoing common alert popup into the app
   * Heading and content will be passwd as arguments
   * @param popHead
   * @param popContent
   */
  showAlertPopup(popHead,popContent){
    let alert = this.alertCtrl.create({
      title: popHead,
      subTitle: popContent,
      buttons: ['OK']
    });
    alert.present();
  }
  /**
   * Method for shwoing common Toast popup into the app
   * Message Data,Duration,positiom will be passwd as arguments
   * @param data
   * @param duration
   * @param position
   */
  showToastPopup(data,duration,position){
    let toast = this.toastCtrl.create({
      message: data,
      duration: duration,
      position: position
    });
    toast.present();
  }
  /**
   * Method for shwoing Loading
   * Message Data will be passwd as arguments
   * @param data
   */
  showLoading(data){
    this.ldcntrl = this.loading.create({
      content: data,
      enableBackdropDismiss:false//Whether the loading indicator should be dismissed by tapping the backdrop. Default false.
    });
    this.ldcntrl.present();
  }
  /**
   * Method for showing netowrk error alert message
   * triggers whenever netowrk connection got lost during API calls
  */
  showNetwrokErrorAlert(){
    let msg = this.utils.getMessageConfigs("internet");
    let altc = this.alertCtrl.create({
      title:msg[0].title,
      subTitle:msg[0].message,
      buttons:[{
        text:"Exit",
        handler:()=>{
          this.platform.exitApp();
        }
      },{
        text:"Retry",
        handler:()=>{
          window.location.reload;
        }
      }]
    });
    altc.present();
  }
  /**
   * Method for dismiss the loader
   */
  dismissLoader(){
    this.ldcntrl.dismiss();
  }
}
