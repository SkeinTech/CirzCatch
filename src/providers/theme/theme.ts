import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';

@Injectable()
export class ThemeProvider {
  public theme: BehaviorSubject<String>;

  constructor(public http: HttpClient) {
    console.log('Hello ThemeProvider Provider');
    this.theme = new BehaviorSubject('light');
  }
  /**
   * Method for set active theme
   * @param val 
   */
  setActiveTheme(val) {
    this.theme.next(val);
  }
  
  /** 
   * Method for getting Active theme
  */
  getActiveTheme() {
    return this.theme.asObservable();
  }

}
