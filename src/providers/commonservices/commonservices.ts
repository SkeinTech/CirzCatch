import { ENV } from '@app/env';
import { Network } from '@ionic-native/network';
import { CommondialoguesProvider } from './../commondialogues/commondialogues';
import { CommonutilsProvider } from './../commonutils/commonutils';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/**
 * Common provider sending/getting request,response to API service from app
 * It's a service layer for accessing from/to services from backend server
 * @author ArulSelvam
 * @date 14.02.2018
 */
declare var navigator;
@Injectable()
export class CommonservicesProvider {
  apiUrl: any = "https://www.skeinlab.com/crizcatch";
  message: any = "";

  constructor(
    public http: Http,
    public utils: CommonutilsProvider,
    public network: Network,
    public dialogues: CommondialoguesProvider) {
    console.log(' CommonservicesProvider Provider');
  }
  /**
   * Method for building headers
   */
  buildHeaders() {
    let headers;
    headers = new Headers({
      'Content-Type': 'application/json',
    });
    return headers;
  }
  /**
   * Method for invoking service from app
   * Based on Method Type passing, invoke the method the calls
   * @param methodType
   * @param path
   * @param payload
   */
  invokeService(methodType, path, payload) {
    // return (methodType == 'GET') ? this.getMethod(path,payload) : this.postMethod(path,payload);
    let serviceCall: any = "";
    switch (methodType) {
      case 'GET':
        serviceCall = this.getMethod(path, payload);
        break;
      case 'POST':
        serviceCall = this.postMethod(path, payload);
        break;
      case 'PUT':
        serviceCall = this.putMethod(path, payload);
        break;
    }
    return serviceCall;
  }
  /**
   * Method for GET request calling
   * @param path
   * @param payload
   */
  getMethod(path, payload) {
    let message: any = this.utils.getMessageConfigs("internet");
    let payData = (payload) ? '/'+ payload : '';
    return new Promise(resolve => {
      if (navigator.onLine) {
        this.http.get(this.apiUrl + '/' + path + payData, { headers: this.buildHeaders() }).map((res) => res.json()).subscribe((data) => {
          resolve(data);
        }, (err) => {
          console.log(err);
        });
      } else {
        this.dialogues.showAlertPopup(message[0].heading, message[0].message);
      }
    });
  }
  /**
   * Method for POST request calling
   * @param path
   * @param payload
   */
  postMethod(path, payload) {
    let message: any = this.utils.getMessageConfigs("internet");
    let payData = (payload) ? '/'+ payload : '';
    return new Promise((resolve, reject) => {
      if (navigator.onLine) {
        console.log(this.apiUrl+path+payData);                
        this.http.post(this.apiUrl + '/' + path, payload, { headers: this.buildHeaders() })
          .map((res) => res.json())
          .subscribe((res) => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      } else {
        this.dialogues.showAlertPopup(message[0].heading, message[0].message);
      }
    });
  }
  /**
   * Method for PUT request calling
   * @param path
   * @param payload
   */
  putMethod(path, payload) {
    let message: any = this.utils.getMessageConfigs("internet");
    // let payData = (payload) ? '/' + payload : '';
    return new Promise(resolve => {
      if (navigator.onLine) {
        this.http.put(this.apiUrl + '/' + path, payload, { headers: this.buildHeaders() })
          .map((res) => res.json())
          .subscribe((data) => {
            resolve(data);
          }, (err) => {
          });
      } else {
        this.dialogues.showAlertPopup(message[0].heading, message[0].message);
      }
    });
  }
}
