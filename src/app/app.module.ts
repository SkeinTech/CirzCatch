import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule, Http } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';

/**
 * Section for ionic native plugins
 */
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Screenshot } from '@ionic-native/screenshot';
import { IonicStorageModule } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { IonicImageViewerModule } from 'ionic-img-viewer';

/**
 * Custom Provider section
 * importing manually generated providers
 */
import { CommonstorageProvider } from '../providers/commonstorage/commonstorage';

/**
 * Section for Page,Since Lazy loading is using,
 * importing pages are not allowed in this project
 */
import { MyApp } from './app.component';
import { CommonservicesProvider } from '../providers/commonservices/commonservices';
import { CommondialoguesProvider } from '../providers/commondialogues/commondialogues';
import { CommonutilsProvider } from '../providers/commonutils/commonutils';
import { ThemeProvider } from '../providers/theme/theme';
import { CommonModule } from '@angular/common';

/**Environment Variable configuration */
import { ENV } from '@app/env';
@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    HttpModule,
    IonicImageViewerModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CommonstorageProvider,
    CommonservicesProvider,
    CommondialoguesProvider,
    CommonutilsProvider,
    SocialSharing,
    ThemeProvider,
    Screenshot,
    Camera,
    CommonModule,
    DatePipe,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
