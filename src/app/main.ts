import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';
import { AppModule } from './app.module';
/**
 * Have a good boost on performance and speed of your Ionic 2 app :
 * the device ready event will fire much sooner
 */
enableProdMode();
platformBrowserDynamic().bootstrapModule(AppModule);
