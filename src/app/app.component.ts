import { CommonservicesProvider } from './../providers/commonservices/commonservices';
import { CommonutilsProvider } from './../providers/commonutils/commonutils';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, MenuController, Menu, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ThemeProvider } from '../providers/theme/theme';
import { CommondialoguesProvider } from '../providers/commondialogues/commondialogues';
import { CommonstorageProvider } from '../providers/commonstorage/commonstorage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  selectedTheme: String;
  loginData:any="";
  rootPage: any = 'RegistrationPage';

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public menu:MenuController,
    public dialog : CommondialoguesProvider,
    public service : CommonservicesProvider,
    public storage:CommonstorageProvider,
    public utils:CommonutilsProvider,
    public mytheme: ThemeProvider,
    public alert:AlertController,
    public splashScreen: SplashScreen,
    public events: Events) {
    this.initializeApp();
    this.loginData = [];
    this.mytheme.getActiveTheme().subscribe(val => this.selectedTheme = val);
     events.subscribe('user:updated',() => {
       console.log("Login Data event");
      //  this.validateandLogin();
      this.editUserDetails();
   });
  }
  /**
   * Method for initializing and ready the cordova platform
   * all app codes will be started here
  */
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
     // this.statusBar.overlaysWebView(true);
      this.statusBar.backgroundColorByHexString("#004ba0");
      this.splashScreen.hide();
      //this.getTeamDetail();
      this.validateandLogin();
    });
  }
  /**
   * Method for reading the data  from storage and navigate process
   * Based on local storage data,navigating to login and home page
  */
  validateandLogin(){
    this.storage.decryptandRead("loginData").then((respData:any)=>{
      let login = JSON.parse(respData);
      this.loginData = (login) ? login[0] : "";
      console.log("Login data from local storage",this.loginData);
      (this.loginData) ? this.nav.setRoot("HomePage") : this.nav.setRoot("RegistrationPage");
    });
  }

  /** Get Team Name and Team ID  GET Method*/
  getTeamDetail(){
    let servicePath = this.utils.getApiConfigs("getTeam");
    this.service.invokeService(servicePath[0].method,servicePath[0].path,'').then((respData:any) =>{
      console.log(respData);
      if(respData.code ==200){
      console.log(respData);
      }
    });
  }
  /**
   * Method for navigating pages
   * based on root settings, pages will be set root  or push into navigation stack
   * @param data
   * @param root
   */
  navigatePages(data,root){
    console.log("Page data is",data);
    if(data == 'RegistrationPage'){
      let altc = this.alert.create({
        title: 'Logout App',
        message: 'Are you really want to logout?',
        buttons: [
          {
            text: 'No',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Yes',
            handler: () => {
              console.log('agree clicked');
              this.menu.close();
              this.storage.removeItem('loginData');
              this.nav.setRoot(data);
            }
          }
        ]
      });
      altc.present();
    }else{
    // (data == 'Scoreboard' || data == 'Settings' || data == 'Profile') ?
    // this.dialog.showAlertPopup("Information","Currently We are working on this feature, it will be comming soon!!!"):
    (root == "yes")? this.nav.setRoot(data) : this.nav.push(data);
    this.menu.close();
    }
  }
  editUserDetails(){
    this.storage.decryptandRead("loginData").then((respData:any)=>{
      let login = JSON.parse(respData);
      this.loginData = (login) ? login[0] : "";
      console.log("Login data from local storage",this.loginData);
    });
  }
}
