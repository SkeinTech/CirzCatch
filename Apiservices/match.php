<?php
    // Connect to database
	$connection=mysqli_connect('localhost','root','','db_crizcatch');

	$request_method=$_SERVER["REQUEST_METHOD"];
	switch($request_method)
	{    
		case 'GET':
				get_match();
		break;
		case 'POST':
			insert_match();
		break;
	}
    function insert_match()
	{
		global $connection;
		global $response;	
		global $row;
		global $finalOut;
		$str_json = file_get_contents('php://input');
		$payload1 = json_encode(array("message" => $str_json));
		$payload = json_decode($str_json,true);		
		
		$match_id = $payload["match_id"];
		$team_A_id = $payload["team_A_id"];
		$team_B_id = $payload["team_B_id"];
		$team_A_name = $payload["team_A_name"];
		$team_B_name = $payload["team_B_name"];
		$overs = $payload["overs"];
		$query = "INSERT INTO tbl_match (match_id,team_A_id,team_B_id,team_A_name,team_B_name,overs) values ('$match_id','$team_A_id','$team_B_id','$team_A_name','$team_B_name','$overs')";
		
		if(mysqli_query($connection, $query))
		{
			$response[]=$row;
		}		
		
		$finalOut = ($response) ?
		array(
			'code' => 200,
			'message' =>'Match Details Registered Successfully.',
			'payload' => $response
		)  : 
		array(
			'code' => 400,
			'message' =>'Match Details Registered Failed.',
			'payload' => $response
		);
		header('Content-Type: application/json');
		echo json_encode($finalOut,true);
	}	
	function get_match()
	{
		global $connection;
		global $finalOut;
		global $response;
		$str_json = file_get_contents('php://input');
		$payload = json_decode($str_json,true);		
		
		$query = mysqli_query($connection,"select * from tbl_match");
		while($row=mysqli_fetch_object($query))
		{
			$response[]=$row;
		}
		$finalOut = ($response) ?
		array(
			'code' => 200,
			'message' =>'Get Match Details Successfully.',
			'payload' => $response
		)  : 
		array(
			'code' => 400,
			'message' =>'Get Match Details Failed.',
			'payload' => $response
		);
		header('Content-Type: application/json');
		echo json_encode($finalOut,true);
	}
?>