-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 21, 2018 at 05:01 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_crizcatch`
--
CREATE DATABASE IF NOT EXISTS `db_crizcatch` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db_crizcatch`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertUser`(IN `u_name` VARCHAR(500), IN `u_email` VARCHAR(1000), IN `u_mobile` INT(15), IN `u_password` VARCHAR(15))
    NO SQL
insert into tbl_user_registration (
    username,
    email_id,
    mobile_no,
    password
) values (
   u_name,
   u_email,
   u_mobile,
   u_password

)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_match`
--

CREATE TABLE IF NOT EXISTS `tbl_match` (
  `match_id` varchar(15) NOT NULL,
  `team_A_id` bigint(15) NOT NULL,
  `team_B_id` int(15) NOT NULL,
  `team_A_name` varchar(50) NOT NULL,
  `team_B_name` varchar(50) NOT NULL,
  `overs` bigint(10) NOT NULL,
  `match_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`match_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_match`
--

INSERT INTO `tbl_match` (`match_id`, `team_A_id`, `team_B_id`, `team_A_name`, `team_B_name`, `overs`, `match_date`) VALUES
('', 0, 0, '', '', 0, '2018-02-19 11:08:51'),
('1', 2, 3, 'CSK', 'RCB', 20, '2018-02-19 11:10:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_team`
--

CREATE TABLE IF NOT EXISTS `tbl_team` (
  `team_id` varchar(15) NOT NULL,
  `team_name` varchar(50) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_team`
--

INSERT INTO `tbl_team` (`team_id`, `team_name`) VALUES
('', ''),
('0', ''),
('10', 'KKR'),
('11', 'KKR'),
('2', 'CSK'),
('4', 'CSK'),
('TMNcsk', 'csk'),
('TMNkxip', 'kxip'),
('TMNrcb', 'rcb'),
('TMNRR', 'RR');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_team_registration`
--

CREATE TABLE IF NOT EXISTS `tbl_team_registration` (
  `team_id` bigint(15) NOT NULL,
  `team_name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `age` bigint(10) NOT NULL,
  `role` varchar(25) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_team_registration`
--

INSERT INTO `tbl_team_registration` (`team_id`, `team_name`, `first_name`, `last_name`, `age`, `role`) VALUES
(0, 'Array', 'sathish', 'mk', 23, 'Array'),
(4, 'CSK', '', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_toss`
--

CREATE TABLE IF NOT EXISTS `tbl_toss` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `team_id` varchar(15) NOT NULL,
  `team_name` varchar(50) NOT NULL,
  `choose_toss` varchar(50) NOT NULL,
  `choice` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_registration`
--

CREATE TABLE IF NOT EXISTS `tbl_user_registration` (
  `user_id` bigint(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email_id` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mobile_no` bigint(15) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_user_registration`
--

INSERT INTO `tbl_user_registration` (`user_id`, `username`, `email_id`, `password`, `mobile_no`) VALUES
(1, 'arul', 'arul@gmail.com', 'arul2018', 9876543210);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
