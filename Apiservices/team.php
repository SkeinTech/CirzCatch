<?php
    // Connect to database
    $connection=mysqli_connect('localhost','root','','db_crizcatch');

	$request_method=$_SERVER["REQUEST_METHOD"];
	switch($request_method)
	{    
		case 'GET':
				get_team();
		break;
		case 'POST':
			insert_team();
		break;
	}
    function insert_team()
	{
		global $connection;
		global $response;	
		global $row;
		global $finalOut;
		$str_json = file_get_contents('php://input');
		$payload1 = json_encode(array("message" => $str_json));
		$payload = json_decode($str_json,true);		
		
		$team_id = $payload["team_id"];
		$team_name = $payload["team_name"];
		$query = "INSERT INTO tbl_team (team_id,team_name) values ('$team_id','$team_name')";
		
		if(mysqli_query($connection, $query))
		{
			$response[]=$row;
		}		
		
		$finalOut = ($response) ?
		array(
			'code' => 200,
			'message' =>'Team Register Successfully.',
			'payload' => $response
		)  : 
		array(
			'code' => 400,
			'message' =>'Team Register Failed or ID Already Existed.',
			'payload' => $response
		);
		header('Content-Type: application/json');
		echo json_encode($finalOut,true);
	}	
	function get_team()
	{
		global $connection;
		global $finalOut;
		global $response;
		$str_json = file_get_contents('php://input');
		$payload = json_decode($str_json,true);		
		
		$query = mysqli_query($connection,"select * from tbl_team");
		while($row=mysqli_fetch_object($query))
		{
			$response[]=$row;
		}
		$finalOut = ($response) ?
		array(
			'code' => 200,
			'message' =>'Get Team Details Successfully.',
			'payload' => $response
		)  : 
		array(
			'code' => 400,
			'message' =>'Get Team Details Failed.',
			'payload' => $response
		);
		header('Content-Type: application/json');
		echo json_encode($finalOut,true);
	}
?>